/*jshint esversion: 6 */

/* ############################################################################ 

Kurs «Generative Gestaltung» an der TH Köln
Christian Noss
christian.noss@th-koeln.de
https://twitter.com/cnoss
https://cnoss.github.io/generative-gestaltung/

############################################################################ */


let saveParams = {
  sketchName: "gg-sketch"
}


// Params for canvas
let canvasParams = {
  holder: document.getElementById('canvas'),
  state: false,
  mouseX: false,
  mouseY: false,
  mouseLock: false,
  background: 'rgba(0,0,0,0)',
  w: 1000,
  h: 800,
  gui: true,
  mode: 'canvas', // canvas or svg … SVG mode is experimental 
};
getCanvasHolderSize();

// Params for the drawing
let drawingParams = {
  pixelkachelgroesse: 10,
  pixelkachelgroesseMin: 10,
  pixelkachelgroesseMax: 100,
  pixelkachelgroesseStep: 1,

  figuresize: 10,
  figuresizeMin: 10,
  figuresizeMax: 100,
  figuresizeStep: 1,

  randomness: 1,
  randomnessMin: 1,
  randomnessMax: 200,
  randomnessStep: 1
};

// Helping variables 
let canvasResize = false;
let imageLoaded = false;
let globalImageObj;
let globalContext;
let globalCanvas; 
let randomMode = false;

// Params for logging
let loggingParams = {
  targetDrawingParams: document.getElementById('drawingParams'),
  targetCanvasParams: document.getElementById('canvasParams'),
  state: false
};






/* ###########################################################################
Classes
############################################################################ */
var Punkt = class {
  constructor(x, y, r, g, b, a) {
      this.x = x;
      this.y = y;
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
  }

  // Adding a method to the constructor
  draw() {
      return `${this.name} says hello.`;
  }
}




/* ###########################################################################
Custom Functions
############################################################################ */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

function drawPoints(context, imageObj) {
  //
  var width = imageObj.width;
  var height = imageObj.height;
  var anzahlwidth = Math.round(width/drawingParams.pixelkachelgroesse);
  var anzahlheight = Math.round(height/drawingParams.pixelkachelgroesse);
  var x1 = 0;
  var y1 = 0;
  var x2 = 0;
  var y2 = 0; 

  for(var i=0; i<anzahlheight; i++) {
    for(var j=0; j<anzahlwidth; j++) {

      var imageData = context.getImageData(j,i,1,1);
      var data = imageData.data
      var r = data[0];
      var g = data[1];
      var b = data[2];
      var a = data[3];

      stroke('rgba('+r+','+g+','+b+','+a/255+')');
      
      strokeWeight(drawingParams.figuresize);
      let addRandomX = 0;
      let addRandomY = 0;
      if(randomMode) {
        addRandomX = getRandomInt((-1)*drawingParams.randomness, drawingParams.randomness);
        addRandomY = getRandomInt((-1)*drawingParams.randomness, drawingParams.randomness);        
      }
      point(j * Math.round(drawingParams.pixelkachelgroesse) + Math.round(drawingParams.pixelkachelgroesse)/2 + addRandomX, i * Math.round(drawingParams.pixelkachelgroesse) + Math.round(drawingParams.pixelkachelgroesse)/2 + addRandomY);
    }
  }
}

function updateCanvas(canvasmonk, contextmonk, imageObj) {
  var width = imageObj.width;
  var height = imageObj.height;

  var anzahlwidth = Math.round(width/drawingParams.pixelkachelgroesse);
  var anzahlheight = Math.round(height/drawingParams.pixelkachelgroesse);

  canvasmonk.width = anzahlwidth;
  canvasmonk.height = anzahlheight;

  contextmonk.drawImage(imageObj, 0, 0, anzahlwidth, anzahlheight);
}



/* ###########################################################################
P5 Functions
############################################################################ */



function setup() {

  let canvas;
  if (canvasParams.mode === 'svg') {
    canvas = createCanvas(canvasParams.w, canvasParams.h, SVG);
  } else { 
    canvas = createCanvas(canvasParams.w, canvasParams.h);
    canvas.parent("canvas");
  }

  // Display & Render Options
  frameRate(5);
  //noLoop();
  angleMode(DEGREES);
  smooth();

  // GUI Management
  if (canvasParams.gui) { 
    let sketchGUI = createGui('Params');
    sketchGUI.addObject(drawingParams);
    //noLoop();
  }

  // Anything else
  fill(255);
  stroke(255);
}



function draw() {

  /* ----------------------------------------------------------------------- */
  // Log globals
  if (!canvasParams.mouseLock) {
    canvasParams.mouseX = mouseX;
    canvasParams.mouseY = mouseY;
    logInfo();
  }

  /* ----------------------------------------------------------------------- */
  // Provide your Code below
  

  if(!canvasParams.mouseLock)
  {

    var canvasmonk = document.createElement('canvas');
    var contextmonk = canvasmonk.getContext('2d');
    
    if(!imageLoaded) {
      var imageObj = new Image();
      imageObj.onload = function()
      {
        
        // updateCanvas(contextmonk, imageObj);
          globalContext = contextmonk;
          globalImageObj = imageObj;
          globalCanvas = canvasmonk;
          
          imageLoaded = true;
      };
      imageObj.src = 'nike.png';
    } else {
      if(globalImageObj != undefined)
      {
        if(!canvasResize) {
          resizeCanvas(globalImageObj.width, globalImageObj.height, true);
        }
        updateCanvas(globalCanvas, globalContext, globalImageObj);
        background(canvasParams.background);
        drawPoints(globalContext, globalImageObj);
      }
    }
  }
  

}



function keyPressed() {

  if (keyCode === 82) { // R-Key
    randomMode = !randomMode;
    console.log(randomMode);
  }

  if (keyCode === 81) { // Q-Key
  }

  if (keyCode === 87) { // W-Key
  }

  if (keyCode === 89) { // Y-Key
  }

  if (keyCode === 88) { // X-Key
  }

  if (keyCode === 83) { // S-Key

    let suffix = (canvasParams.mode === "canvas") ? '.jpg' : '.svg';
    save(saveParams.sketchName + suffix);
  }

  if (keyCode === 49) { // 1-Key
  }

  if (keyCode === 50) { // 2-Key
  }

  if (keyCode === 76) { // L-Key
    if (!canvasParams.mouseLock) {
      canvasParams.mouseLock = true;
    } else { 
      canvasParams.mouseLock = false;
    }
    document.getElementById("canvas").classList.toggle("mouseLockActive");
  }


}



function mousePressed() {}



function mouseReleased() {}



function mouseDragged() {}



function keyReleased() {
  if (keyCode == DELETE || keyCode == BACKSPACE) clear();
}





/* ###########################################################################
Service Functions
############################################################################ */



function getCanvasHolderSize() {
  canvasParams.w = canvasParams.holder.clientWidth;
  canvasParams.h = canvasParams.holder.clientHeight;
}



function resizeMyCanvas() {
  getCanvasHolderSize();
  resizeCanvas(canvasParams.w, canvasParams.h);
}



function windowResized() {
  resizeMyCanvas();
}



function logInfo(content) {

  if (loggingParams.targetDrawingParams) {
    loggingParams.targetDrawingParams.innerHTML = helperPrettifyLogs(drawingParams);
  }

  if (loggingParams.targetCanvasParams) {
    loggingParams.targetCanvasParams.innerHTML = helperPrettifyLogs(canvasParams);
  }

}


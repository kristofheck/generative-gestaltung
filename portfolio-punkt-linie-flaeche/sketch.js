/*jshint esversion: 6 */

/* ############################################################################ 

Kurs «Generative Gestaltung» an der TH Köln
Christian Noss
christian.noss@th-koeln.de
https://twitter.com/cnoss
https://cnoss.github.io/generative-gestaltung/

############################################################################ */


let saveParams = {
  sketchName: "gg-sketch"
}


// Params for canvas
let canvasParams = {
  holder: document.getElementById('canvas'),
  state: false,
  mouseX: false,
  mouseY: false,
  mouseLock: false,
  background: 200,
  gui: true,
  mode: 'svg', // canvas or svg … SVG mode is experimental 
};
getCanvasHolderSize();

// Params for the drawing
let drawingParams = {
  maxSize: 100,
  maxSizeMin: 5,
  maxSizeMax: 500,
  maxSizeStep: 1,

  minSize: 5,
  minSizeMin: 5,
  minSizeMax: 500,
  minSizeStep: 1
};

// Params for logging
let loggingParams = {
  targetDrawingParams: document.getElementById('drawingParams'),
  targetCanvasParams: document.getElementById('canvasParams'),
  state: false
};

var formArray = [];
var reset = false;



/* ###########################################################################
Classes
############################################################################ */

class Form {
  constructor(farbe) {
    this.pfad = [];
    this.pfadgezeichnet = false;
    this.farbe = farbe;
    this.index = 0;
    this.size = getRandomInt(drawingParams.minSize,drawingParams.maxSize);
    this.maxSize = getRandomInt(drawingParams.minSize,drawingParams.maxSize);
    this.richtung = getRandomInt(0,1);
  }

  addPoint(x, y) {
    var newPoint = [x, y];
    this.pfad.push(newPoint);
  }

  /*
  drawpfad() {
    if(!this.pfadgezeichnet) {
      for(var i=1; i<this.pfad.length; i++) {
        var p1 = this.pfad[i-1];
        var p2 = this.pfad[i];
        stroke(0);
        line(p1[0], p1[1], p2[0], p2[1]);
      }
      this.pfadgezeichnet = true;
    }
  }
  */

  update() {
    var punkt = this.pfad[this.index];
    if(this.index + 1 < this.pfad.length) this.index++;
    if(this.index + 1 == this.pfad.length) return;
    var punktX = punkt[0];
    var punktY = punkt[1];
    

    stroke(this.farbe);
    circle(punktX, punktY, this.size);
    

    if(this.richtung == 1) {
      this.size += getRandomInt(1,5);
    }

    if(this.richtung == 0) {
      this.size -= getRandomInt(1,5);
    }

    if(this.size >= this.maxSize) {
      this.richtung = 0;
    }

    if(this.size <= drawingParams.minSize) {
      this.richtung = 1;
    }
  }
}



/* ###########################################################################
Custom Functions
############################################################################ */

function getRandomInt(min, max) {
  var rnd = Math.floor(Math.random() * (max - min) ) + min;
  if(min<0 && rnd == 0)
  {
    return getRandomInt(min, max);
  }
  return rnd;
}



/* ###########################################################################
P5 Functions
############################################################################ */



function setup() {

  let canvas;
  if (canvasParams.mode === 'svg') {
    canvas = createCanvas(canvasParams.w, canvasParams.h, SVG);
  } else { 
    canvas = createCanvas(canvasParams.w, canvasParams.h);
    canvas.parent("canvas");
  }
  fill(0, 10);
  colorMode(HSB);
  // Display & Render Options
  frameRate(25);
  angleMode(DEGREES);
  smooth();
  background(0, 0, 100);

  // GUI Management
  if (canvasParams.gui) { 
    let sketchGUI = createGui('Params');
    sketchGUI.addObject(drawingParams);
    //noLoop();
  }

  background(canvasParams.background);

  // Anything else
}



function draw() {

  /* ----------------------------------------------------------------------- */
  // Log globals
  if (!canvasParams.mouseLock) {
    canvasParams.mouseX = mouseX;
    canvasParams.mouseY = mouseY;
    logInfo();
  }

  /* ----------------------------------------------------------------------- */
  // Provide your Code below
  
  
  formArray.forEach(function(form) {
    //form.drawpfad();
    form.update();
  });

  if(reset) {
    background(canvasParams.background);
    formArray = [];
    reset = false;
  }
}



function keyPressed() {

  if (keyCode === 81) { // Q-Key
  }

  if (keyCode === 82) { // R-Key
    reset = true;
  }

  if (keyCode === 87) { // W-Key
  }

  if (keyCode === 89) { // Y-Key
  }

  if (keyCode === 88) { // X-Key
  }

  if (keyCode === 83) { // S-Key

    let suffix = (canvasParams.mode === "canvas") ? '.jpg' : '.svg';
    save(saveParams.sketchName + suffix);
  }

  if (keyCode === 49) { // 1-Key
  }

  if (keyCode === 50) { // 2-Key
  }

  if (keyCode === 76) { // L-Key
    if (!canvasParams.mouseLock) {
      canvasParams.mouseLock = true;
    } else { 
      canvasParams.mouseLock = false;
    }
    document.getElementById("canvas").classList.toggle("mouseLockActive");
  }


}



function mousePressed() {
  newForm = new Form(color(random(360), 80, 60, 50));
}

function mouseReleased() {
  formArray.push(newForm);
  console.log(formArray);
}



function mouseDragged() {
  
  newForm.addPoint(mouseX, mouseY);
}



function keyReleased() {
  if (keyCode == DELETE || keyCode == BACKSPACE) clear();
}





/* ###########################################################################
Service Functions
############################################################################ */



function getCanvasHolderSize() {
  canvasParams.w = canvasParams.holder.clientWidth;
  canvasParams.h = canvasParams.holder.clientHeight;
}



function resizeMyCanvas() {
  getCanvasHolderSize();
  resizeCanvas(canvasParams.w, canvasParams.h);
}



function windowResized() {
  resizeMyCanvas();
}



function logInfo(content) {

  if (loggingParams.targetDrawingParams) {
    loggingParams.targetDrawingParams.innerHTML = helperPrettifyLogs(drawingParams);
  }

  if (loggingParams.targetCanvasParams) {
    loggingParams.targetCanvasParams.innerHTML = helperPrettifyLogs(canvasParams);
  }

}

